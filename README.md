![Earth Texture](https://codeberg.org/jibran.syed/Procedutex/raw/branch/master/Screenshots/tex_earth.png)

**Notice this code is from several years ago. It might no longer work.**

# Procedutex
A Object Oriented 2D Procedural Texture Generation Library

![Test Window Screenshot](https://codeberg.org/jibran.syed/Procedutex/raw/branch/master/Screenshots/test_window.png)

Requires Visual Studios 2013 or newer. Procedutex is a library that compiles into static libraries (.lib files). These lib files are tested in separate projects called ProcedutexTestWindow and DemoEngine.

The ProcedutexTestWindow is a simple window showing 4 textures made with the procedural library. The test engine is a custom C++ game engine that was being made from another academic project to test Procedutex on 3D objects. For the DemoEngine project, it is recommended to run on Release mode; the program takes too long to load on Debug mode.

![Demo Engine Screenshot](https://codeberg.org/jibran.syed/Procedutex/raw/branch/master/Screenshots/demo_engine.png)
